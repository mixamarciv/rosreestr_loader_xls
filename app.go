package main

import (
	//"bytes"

	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
	//"text/template"
	//"time"

	//xmlx "github.com/jteeuwen/go-pkg-xmlx"
	mf "github.com/mixamarciv/gofncstd3000"

	//"os"

	//flags "github.com/jessevdk/go-flags"

	//"net/http/cookiejar"

	//xlsreader "github.com/extrame/xls"
	//xlsreader "github.com/tealeg/xlsx"
	//xlsreader "github.com/nivrrex/excel"
	xlsx "github.com/tealeg/xlsx"
)

var Fmts = fmt.Sprintf
var Print = fmt.Print
var Itoa = strconv.Itoa
var apppath string
var url_host string = "https://rosreestr.ru"

func main() {
	InitLog()
	apppath, _ = mf.AppPath()
	apppath = strings.Replace(apppath, "\\", "/", -1)

	p1 := make(chan int)
	//p2 := make(chan int)
	//p3 := make(chan int)
	go func() {
		//p3 <- 0
		//p2 <- 0
		p1 <- 0
	}()

	infiles := load_in_files_list()
	//LogPrintAndExit(fmt.Sprintf("%#v", in))

	for _, file := range infiles {
		work_with_in_xls_file(file)
	}

	//<-p3
	//<-p2
	<-p1

}

//загружаем список файлов которые будем обрабатывать
func load_in_files_list() []string {
	LogPrint("загружаем список файлов для обработки")
	inlistpath := apppath + "/in"
	files, err := ioutil.ReadDir(inlistpath)
	LogPrintErrAndExit("ERROR008 ReadDir: \""+inlistpath+"\"\n", err)

	arrfiles := make([]string, 0)
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		name := file.Name()
		LogPrint(name)
		if mf.StrRegexpMatch("\\.xlsx$", name) == false {
			continue
		}
		arrfiles = append(arrfiles, name)
	}
	return arrfiles
}

//читаем xls файл, получаем и выгружаем данные в другой xls файл
func work_with_in_xls_file(filename string) {
	LogPrint("обработка " + filename)
	mf.MkdirAll(apppath + "/out")

	log_file_old := log_file
	log_file = apppath + "/out/" + filename + ".log" //переопределяем лог файл

	/**************
	//xlsreader "github.com/nivrrex/excel"
		e := &xlsreader.Excel{Visible: false, Readonly: false, Saved: true}
		e.Open(filePath)
		fmt.Println(e.Cells(1, 1))
		e.Close()
	****************/
	/***************
	//xlsreader "github.com/tealeg/xlsx"
		xlFile, err := xlsreader.OpenFile(apppath + "/in/" + filename)
		LogPrintErrAndExit("ОШИБКА открытия xls файла: \""+apppath+"/in/"+filename+"\"\n", err)
		sheet := xlFile.Sheets[0]

		for _, row := range sheet.Rows {
			for i, cell := range row.Cells {
				if i > 5 {
					break
				}
				s, _ := cell.String()
				fmt.Printf("%s\n", s)
			}
		}
	***************/
	/***************
	//xlsreader "github.com/extrame/xls"
		xlFile, err := xlsreader.Open(apppath+"/in/"+filename, "utf-8")
		LogPrintErrAndExit("ОШИБКА чтения xls файла: \""+apppath+"/in/"+filename+"\"\n", err)

		sheet := xlFile.GetSheet(0)
		LogPrint(fmt.Sprintf("читаем лист \"%s\", всего строк: %d", sheet.Name, sheet.MaxRow))

		//xlFile.PrepareSheet(sheet)

		col1 := sheet.Rows[0].Cols[0]
		col2 := sheet.Rows[0].Cols[0]
		for i := 0; i <= (int(sheet.MaxRow)); i++ {
			LogPrint(fmt.Sprintf("строка %d", i))

			row := sheet.Rows[uint16(i)]
			//for n, col := range row.Cols {
			//	fmt.Println(n, "==>", col.String(xlFile), " ")
			//}
			if len(row.Cols) < 5 {
				continue
			}
			//row.Cols
			col1 = row.Cols[0]
			col2 = row.Cols[1]

			s1 := col1.String(xlFile)
			s2 := col2.String(xlFile)
			LogPrint(fmt.Sprintf("%s %s", s1, s2))
		}
		LogPrint(fmt.Sprintf("%v %v", col1, col2))
	***********/

	//x := new xlsreader
	xlFile, err := xlsx.OpenFile(apppath + "/in/" + filename)
	LogPrintErrAndExit("ОШИБКА007 открытия xlsx файла: \""+apppath+"/in/"+filename+"\"\n", err)
	sheet := xlFile.Sheets[0]

	xltype := "undefined"
	maxcolsneed := 0
	var fields []string

	{ //определяем тип екселевского файла
		row := sheet.Rows[3]

		s := row_cells_to_arr_string(row.Cells, 5)
		fields = s

		if len(s) >= 4 && mf.StrTrim(s[0]) == "Помещение" /*mf.StrTrim(s[0]) == "Код улицы" && mf.StrTrim(s[1]) == "Код" && mf.StrTrim(s[2]) == "Код" &&*/ {
			xltype = "flats"
			maxcolsneed = 4
			fields = fields[0:4]
		} else if len(s) >= 3 && mf.StrTrim(s[0]) == "Здание" /*&& mf.StrTrim(s[1]) == "Код" && mf.StrTrim(s[2]) == "Код улицы"*/ {
			xltype = "houses"
			maxcolsneed = 3
			fields = fields[0:3]
			//LogPrint(fmt.Sprintf("fields: %v", fields))
		} else {
			LogPrint(fmt.Sprintf("%v", s))
		}
	}
	if xltype == "undefined" {
		LogPrintAndExit("ОШИБКА006 тип файла не определен (неизвестно что именно нужно загружать - данные по домам или квартирам)")
	}

	LogPrint("file type: " + xltype + "  cols: " + fmt.Sprintf("%d %v", maxcolsneed, fields))

	arr := make(map[string]interface{})
	arr["xltype"] = xltype
	arr["maxcolsneed"] = maxcolsneed
	arr["fields"] = fields

	{ //создаем новый файл в который будем загружать данные
		file := xlsx.NewFile()
		sheetout, err := file.AddSheet("Sheet1")
		LogPrintErrAndExit("ОШИБКА005 открытия создания листа в файле: \""+apppath+"/out/"+filename+"\"\n", err)

		//row := sheetout.AddRow()
		//cell := row.AddCell()
		//cell.Value = "I am a cell!"
		start_work(sheet, sheetout, arr)

		err = file.Save(apppath + "/out/" + filename)
		LogPrintErrAndExit("ОШИБКА004 сохранения файла: \""+apppath+"/out/"+filename+"\"\n", err)
	}

	log_file = log_file_old
}

//возвращает поля maxcols столбцов строки cells
func row_cells_to_arr_string(cells []*xlsx.Cell, maxcols int) []string {
	a := make([]string, 0)
	for i, cell := range cells {
		if i > maxcols {
			break
		}
		s, _ := cell.String()
		a = append(a, mf.StrTrim(s))
		//fmt.Printf("%s\n", s)
	}
	return a
}

//задает значения полей указанной строки
func set_cells_values_from_arr_string(row *xlsx.Row, values []string) {
	for _, val := range values {
		cell := row.AddCell()
		cell.Value = val
	}
}

//чтение строк в in листе, загрузка данных по ним, и запись в out лист
func start_work(in, out *xlsx.Sheet, arr map[string]interface{}) {
	xltype := arr["xltype"].(string)
	maxcolsneed := arr["maxcolsneed"].(int)

	//задаем титул
	row := out.AddRow()
	row = out.AddRow()
	cell := row.AddCell()
	cell.Value = "список"
	row = out.AddRow()

	//задаем поля:
	row = out.AddRow()
	fields := arr["fields"].([]string)
	if xltype == "houses" {
		fields = append(fields, "Кадастровый номер")                   // a["kadastrn"])     //Кадастровый номер
		fields = append(fields, "Дата постановки на кадастровый учет") // a["date_kadastr"]) //Дата постановки на кадастровый учет
		fields = append(fields, "Дата снятия с учета")                 // a["date_kadastr_end"]) //Дата снятия с учета
		fields = append(fields, "Статус объекта")                      // a["status"])           //Статус объекта
		fields = append(fields, "Адрес (местоположение)")              // a["adres"])            //Адрес (местоположение)
		fields = append(fields, "(ОКС) Тип")                           // a["type"])         //(ОКС) Тип
		fields = append(fields, "Площадь ОКС")                         // a["ob_area"])      //Площадь ОКС
		fields = append(fields, "Дата обновления информации")          // a["date_update"])  //Дата обновления информации
		fields = append(fields, "Кадастровая стоимость")               // a["price"])        //Кадастровая стоимость
		fields = append(fields, "Дата утверждения стоимости")          // a["price_date"])   //Дата утверждения стоимости
		fields = append(fields, "Дата внесения стоимости")             // a["price_date_enter"]) //Дата внесения стоимости
		fields = append(fields, "Дата определения стоимости")          // a["price_date_check"]) //Дата определения стоимости
		fields = append(fields, "Этажность")                           // a["floor_cnt"])    //Этажность
		fields = append(fields, "Подземные этажи")                     // a["ufloor_cnt"])   //Подземные этажи
		fields = append(fields, "Материал стен")                       // a["wall_info"])    //Материал стен
		fields = append(fields, "Завершение строительства")            // a["build_year"])   //Завершение строительства
		fields = append(fields, "Ввод в эксплуатацию")                 // a["build_year2"])  //Ввод в эксплуатацию
	} else if xltype == "flats" {
		fields = append(fields, "Кадастровый номер")                   // a["kadastrn"])     //Кадастровый номер
		fields = append(fields, "Дата постановки на кадастровый учет") // a["date_kadastr"]) //Дата постановки на кадастровый учет
		fields = append(fields, "Дата снятия с учета")                 // a["date_kadastr_end"]) //Дата снятия с учета
		fields = append(fields, "Статус объекта")                      // a["status"])           //Статус объекта
		fields = append(fields, "Адрес (местоположение)")              // a["adres"])            //Адрес (местоположение)
		fields = append(fields, "(ОКС) Тип")                           // a["type"])         //(ОКС) Тип
		fields = append(fields, "Площадь ОКС")                         // a["ob_area"])      //Площадь ОКС
		fields = append(fields, "Дата обновления информации")          // a["date_update"])  //Дата обновления информации
		fields = append(fields, "Кадастровая стоимость")               // a["price"])        //Кадастровая стоимость
		fields = append(fields, "Дата утверждения стоимости")          // a["price_date"])   //Дата утверждения стоимости
		fields = append(fields, "Дата внесения стоимости")             // a["price_date_enter"]) //Дата внесения стоимости
		fields = append(fields, "Дата определения стоимости")          // a["price_date_check"]) //Дата определения стоимости
		//fields = append(fields, "Этажность")                           // a["floor_cnt"])    //Этажность
		//fields = append(fields, "Подземные этажи")                     // a["ufloor_cnt"])   //Подземные этажи
		//fields = append(fields, "Материал стен")                       // a["wall_info"])    //Материал стен
		//fields = append(fields, "Завершение строительства")            // a["build_year"])   //Завершение строительства
		//fields = append(fields, "Ввод в эксплуатацию")                 // a["build_year2"])  //Ввод в эксплуатацию
	}
	set_cells_values_from_arr_string(row, fields)

	//читаем и загружаем данные по одной строке:
	for i, row := range in.Rows {
		if i < 4 {
			continue
		}
		//читаем строку:
		s := row_cells_to_arr_string(row.Cells, maxcolsneed)
		//LogPrint(Fmts("row %d: %v", i, s))
		if len(s) < maxcolsneed {
			continue
		}

		//загружаем данные:
		if xltype == "houses" {
			s = load_data_house(s)
		} else if xltype == "flats" {
			s = load_data_flat(s)
		}

		//записываем в исходящий файл:
		set_cells_values_from_arr_string(out.AddRow(), s)
	}

}

//загружаем данные по домам
func load_data_house(s []string) []string {

	m := make(map[string]string)
	m["street"], m["house"] = str_get_street_house(s[0])
	if m["street"] == "" {
		if strings.ToLower(mf.StrTrim(s[0])) == "итого" {
			return s
		}
		errtext := " ОШИБКА001: невозможно определить номер дома \"" + s[0] + "\""
		LogPrint(errtext)
		return s
	}
	m["type"] = "house"
	m["n_req"] = "0"

	info := m["street"] + "-" + m["house"]
	r := loaditem(info, m, 0)
	if r["error"].(string) != "" {
		errtext := info + " ОШИБКА: " + r["error"].(string)
		LogPrint(errtext)
		s = append(s, errtext)
		return s
	}
	a := r["house_data"].(map[string]string)

	s = append(s, a["kadastrn"])         //Кадастровый номер
	s = append(s, a["date_kadastr"])     //Дата постановки на кадастровый учет
	s = append(s, a["date_kadastr_end"]) //Дата снятия с учета
	s = append(s, a["status"])           //Статус объекта
	s = append(s, a["adres"])            //Адрес (местоположение)
	s = append(s, a["type"])             //(ОКС) Тип
	s = append(s, a["ob_area"])          //Площадь ОКС
	s = append(s, a["date_update"])      //Дата обновления информации
	s = append(s, a["price"])            //Кадастровая стоимость
	s = append(s, a["price_date"])       //Дата утверждения стоимости
	s = append(s, a["price_date_enter"]) //Дата внесения стоимости
	s = append(s, a["price_date_check"]) //Дата определения стоимости
	s = append(s, a["floor_cnt"])        //Этажность
	s = append(s, a["ufloor_cnt"])       //Подземные этажи
	s = append(s, a["wall_info"])        //Материал стен
	s = append(s, a["build_year"])       //Завершение строительства
	s = append(s, a["build_year2"])      //Ввод в эксплуатацию

	return s
}

var tmp_data_house string
var tmp_data_flats map[string]map[string]string = make(map[string]map[string]string) //временное хранилище под загруженные данные по квартирам

//загружаем данные по квартирам
func load_data_flat(s []string) []string {
	//LogPrint(Fmts("\n%#v\n", s))

	m := make(map[string]string)

	street_and_house_and_flat := s[0]
	if strings.ToLower(mf.StrTrim(street_and_house_and_flat)) == "итого" {
		return s
	}
	street_and_house := str_get_steet_house_str(street_and_house_and_flat)
	m["street"], m["house"] = str_get_street_house(street_and_house)
	if m["street"] == "" {
		if strings.ToLower(mf.StrTrim(s[0])) == "итого" {
			return s
		}
		errtext := " ОШИБКА002: невозможно определить номер дома \"" + s[0] + "\""
		LogPrint(errtext)
		return s
	}

	tmp_sh := m["street"] + "-" + m["house"]
	if tmp_data_house != tmp_sh { //если предыдущий раз загружали данные не по этому, то загружаем их по новой
		LogPrint("Загружаем данные по дому [" + tmp_sh + "]:")
		tmp_data_house = tmp_sh

		m["type"] = "flat"
		m["n_req"] = "0"
		info := m["street"] + "-" + m["house"]
		r := loaditem(info, m, 0)
		if r["error"].(string) != "" {
			errtext := info + " ОШИБКА003: " + r["error"].(string)
			LogPrint(errtext)
			s = append(s, errtext)
			return s
		}
		//LogPrint(Fmts("\n%#v\n", r))
		flats := r["flats"].([]map[string]string)
		tmp_data_flats = make(map[string]map[string]string)

		//группируем данные в map[номер квартиры]map[string]string
		for _, f := range flats {
			nflat := f["flat"]
			tmp_data_flats[nflat] = f
		}
		//LogPrint(Fmts("\n%#v\n", tmp_data_flats))
	}

	flat := str_get_flat_number(street_and_house_and_flat)
	a, ok := tmp_data_flats[flat]
	if !ok {
		//LogPrint(Fmts("%s => \"%s\"", street_and_house_and_flat, flat))
		s = append(s, "данные по квартире \""+flat+"\" не найдены")
		return s
	}
	a["loaded"] = "1" //флаг - данные по этой квартире уже выгружены в ексель

	s = append(s, a["kadastrn"])         //Кадастровый номер
	s = append(s, a["date_kadastr"])     //Дата постановки на кадастровый учет
	s = append(s, a["date_kadastr_end"]) //Дата снятия с учета
	s = append(s, a["status"])           //Статус объекта
	s = append(s, a["adres"])            //Адрес (местоположение)
	s = append(s, a["type"])             //(ОКС) Тип
	s = append(s, a["ob_area"])          //Площадь ОКС
	s = append(s, a["date_update"])      //Дата обновления информации
	s = append(s, a["price"])            //Кадастровая стоимость
	s = append(s, a["price_date"])       //Дата утверждения стоимости
	s = append(s, a["price_date_enter"]) //Дата внесения стоимости
	s = append(s, a["price_date_check"]) //Дата определения стоимости
	//s = append(s, a["floor_cnt"])        //Этажность
	//s = append(s, a["ufloor_cnt"])       //Подземные этажи
	//s = append(s, a["wall_info"])        //Материал стен
	//s = append(s, a["build_year"])       //Завершение строительства
	//s = append(s, a["build_year2"])      //Ввод в эксплуатацию

	return s
}

//очищаем стороку с улицей и домом от лишних символов
func clear_street_house_field(s string) string {
	//a := s
	s = strings.ToLower(mf.StrTrim(s))
	s = strings.Replace(s, ", корпус", "", -1)
	s = strings.Replace(s, ", корп.", "", -1)
	s = strings.Replace(s, ", корп", "", -1)

	s = strings.Replace(s, "улица ", "", -1)
	s = strings.Replace(s, "ул,", " ", -1)
	s = strings.Replace(s, " ул.", " ", -1)

	s = strings.Replace(s, "пр - кт", "", -1)
	s = strings.Replace(s, "пр-кт", "", -1)
	s = strings.Replace(s, "проспект,", "", -1)

	s = strings.Replace(s, "дом №", " ", -1)
	s = strings.Replace(s, "дом", " ", -1)
	s = strings.Replace(s, "здание", " ", -1)
	s = strings.Replace(s, "зд.", " ", -1)
	s = strings.Replace(s, "зд ", " ", -1)
	s = strings.Replace(s, " д,", " ", -1)
	s = strings.Replace(s, "д.", " ", -1)

	s = strings.Replace(s, ",", " ", -1)
	s = mf.StrTrim(s)
	s = strings.Replace(s, "  ", " ", -1)
	s = strings.Replace(s, "  ", " ", -1)
	s = strings.Replace(s, "  ", " ", -1)

	s = mf.StrRegexpReplace(s, "(\\d) ([a-zа-я])$", "$1$2")
	s = mf.StrRegexpReplace(s, "(\\d)[ ]*\\([a-zа-я]{2,}\\)$", "$1")
	s = mf.StrRegexpReplace(s, "(\\d)[ ]*[a-zа-я\\. -\"]{2,}$", "$1")
	s = mf.StrRegexpReplace(s, "^пер ", "")
	s = mf.StrRegexpReplace(s, "^ул[\\. ]", "")
	s = mf.StrRegexpReplace(s, " д$", "")
	s = mf.StrRegexpReplace(s, "^пр[\\. ]", "")
	s = mf.StrTrim(s)
	//LogPrint(Fmts("%s \t %s", a, s))

	return s
}

//получаем номер квартиры из строки с адресом
func str_get_flat_number(s string) string {
	s = strings.ToLower(mf.StrTrim(s))
	s = strings.Replace(s, "=", "", -1)
	s = mf.StrRegexpReplace(s, "^.*([\\. ][\\d\\-]+)$", "$1")
	s = mf.StrTrim(s)
	return s
}

//получаем только строку с улицей и домом (просто убераем номер квартиры, все что после последней запятой)
func str_get_steet_house_str(s string) string {
	s = mf.StrTrim(s)
	i := strings.LastIndex(s, ",")
	i2 := strings.LastIndex(s, "кв")
	if i2 != -1 {
		i = i2
	}
	if i <= -1 {
		return s
	}
	s = s[0:i]
	return s
}

//получаем улицу и дом из строки
func str_get_street_house(s string) (string, string) {

	s = clear_street_house_field(s)

	s = mf.StrRegexpReplace(s, " (\\d+[a-zа-я]*)$", "|$1")
	a := strings.Split(s, "|")
	if len(a) < 2 {
		return "", ""
	}
	street := a[0]
	house := a[1]
	return street, house
}
