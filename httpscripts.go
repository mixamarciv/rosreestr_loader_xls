package main

import (
	"bytes"

	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	//"text/template"
	//"time"

	//xmlx "github.com/jteeuwen/go-pkg-xmlx"
	"github.com/PuerkitoBio/goquery"
	mf "github.com/mixamarciv/gofncstd3000"
	//"os"

	//flags "github.com/jessevdk/go-flags"

	"net/http/cookiejar"

	//xlsreader "github.com/extrame/xls"
	//xlsreader "github.com/tealeg/xlsx"
	//xlsreader "github.com/nivrrex/excel"
	//xlsx "github.com/tealeg/xlsx"
)

type urllist struct {
	a []map[string]string
}

func (p *urllist) add(m map[string]string) {
	p.a = append(p.a, m)
}

func getbaseurl(doc *goquery.Document) string {
	sel := doc.Find("base[href]")
	if len(sel.Nodes) == 0 {
		error := "!!!ERROR[ not found base[href] ]"
		LogPrint(error)
		return ""
	}
	href, _ := sel.Attr("href")
	return href
}

func loaditem(info string, m map[string]string, n_req int) map[string]interface{} {
	add_text := ""
	if n_req > 0 {
		add_text = fmt.Sprintf(" %d попытка", n_req+1)
	}
	LogPrint("\n" + info + " загрузка списка " + add_text)

	ret := make(map[string]interface{})

	url := url_host + "/wps/portal/online_request"
	doc, err := goquery.NewDocument(url)
	if err != nil {
		LogPrint(Fmts("%#v", err))
		LogPrintAndExit("request send error: \n url: " + url + "\n\n")
	}

	sel := doc.Find("[method=post]")
	if len(sel.Nodes) == 0 {
		ret["error"] = "not found form[name=search_action]"
		LogPrint(ret["error"].(string))
		return loaditem(info, m, n_req+1)
	}

	action, _ := sel.Attr("action")

	baseurl := getbaseurl(doc)
	//LogPrint("action: " + action)

	//------------------------------------------------------
	//отправляем запрос с сортировкой по адресам(&sortField=ADDRESS_NOTE&sortOrder=asc)
	requrl := baseurl + action + "?sortField=ADDRESS_NOTE&sortOrder=asc"
	//LogPrint(fmt.Sprintf("url_host: %s\naction: %s", url_host, action))
	//LogPrint(fmt.Sprintf("\nrequrl: %s\n", requrl))

	body, d := SendHttpRequestPOST_loadlist(requrl, m)

	{ //проверяем загрузился ли список записей
		b := CheckRequestData_checkloadlist(info, body)
		if !b {
			if m["n_req__change_street"] != "1" {
				m["n_req__change_street"] = "1"
				//меняем местами слова в названии улиц и ещё раз отправляем запрос
				m["street"] = mf.StrRegexpReplace(m["street"], "^([a-zа-я0-9\\-\\/\\\\']+) ([a-zа-я0-9\\-\\/\\\\']+)$", "$2 $1")
				return loaditem(info, m, n_req+1)
			}
		}
		if !b {
			//return
			//тут наверно вывод об ошибке и выход
		}
	}
	//LogPrint("=============================================================================")
	//mf.FileWrite("test/"+info+".url", []byte(requrl))
	//mf.FileWrite("test/"+info+".res", res)
	//LogPrintAndExit(fmt.Sprintf("%#v", string(res)))
	//LogPrint("=============================================================================")

	//-------------------------------------------------------
	//получаем список ссылок со всех страницы результатов
	alist := new(urllist)
	loadpagelinks(info, 1, body, alist, d)

	//mf.FileWrite("test/"+info+".a", []byte(fmt.Sprintf("loaded list: %d\n\n%#v", len(alist.a), alist.a)))
	LogPrint(fmt.Sprintf("loaded list: %d", len(alist.a)))

	//--------------------------------------------------------
	//загружаем и парсим каждую страницу в map[string]string
	ret = map[string]interface{}{"street": m["street"], "house": m["house"], "error": ""}

	data_flats := make([]map[string]string, 0)
	data_houses := make([]map[string]string, 0)

	//может быть несколько кадастровых номеров для одного и того же здания, выбираем первый с максимальной площадью здания
	var house_max_ob_area_zd float64 = -1
	house_ok_i_zd := -1

	//может быть несколько кадастровых номеров для одного и тоже здания, без указания параметра что это здание, выбираем с макс. площадью
	var house_max_ob_area_hz float64 = -1
	house_ok_i_hz := -1

	main_m := m
	for i, m := range alist.a {
		if i > 10 {
			//break
		}

		adres := "[" + m["street"] + ", " + m["house"] + "-" + m["flat"] + "]"
		adres += m["name"]
		url := m["url"]
		if main_m["type"] == "house" {
			if m["flat"] != "" {
				continue
			}
		} else if main_m["type"] == "flat" {
			if m["flat"] == "" || m["flat"] == "-" {
				//continue
				//в любом случае загружаем данные т.к. квартира может быть не указана в адресе но может быть указана в типе помещения!
			}
		}

		t := loadkadastrinfo(info+"  "+adres, url, d)

		if t["type"] == "квартира" || t["flat"] != "-" {
			data_flats = append(data_flats, t)
			LogPrint(fmt.Sprintf(info+" загружены данные по квартире: \"%s\" "+t["kadastrn"]+" "+adres, t["flat"]))
		} else {
			data_houses = append(data_houses, t)
			//LogPrint(fmt.Sprintf(adres+" %v", t))

			//v, _ := strconv.Atoi(t["ob_area"])
			v, _ := strconv.ParseFloat(t["ob_area"], 64)
			if t["type"] == "здание" {
				LogPrint(fmt.Sprintf(info + " загружены данные по зданию: " + t["kadastrn"] + " " + adres))
				if house_ok_i_zd < 0 {
					house_ok_i_zd = len(data_houses) - 1
					house_max_ob_area_zd = v
				}
				if house_max_ob_area_zd < v {
					house_ok_i_zd = len(data_houses) - 1
					house_max_ob_area_zd = v
				}
			} else {
				LogPrint(fmt.Sprintf(info + " загружены данные по " + t["type"] + ": " + t["kadastrn"] + " " + adres))
				if house_max_ob_area_hz < v {
					house_ok_i_hz = len(data_houses) - 1
					house_max_ob_area_hz = v
				}
			}
		}
	}

	ret["flats"] = data_flats

	if house_ok_i_zd >= 0 {
		ret["house_data"] = data_houses[house_ok_i_zd]
	} else {
		if house_ok_i_hz < 0 {
			house_ok_i_hz = 0
			t := loadkadastrinfo_null()
			data_houses = append(data_houses, t)
		}
		ret["house_data"] = data_houses[house_ok_i_hz]
	}

	//LogPrint("=============================================================================")
	LogPrint(fmt.Sprintf("Загружено записей по домам: %d, по квартирам: %d", len(data_houses), len(data_flats)))
	//LogPrint("=============================================================================")

	//LogPrintAndExit("конец")
	return ret
}

//проверяем загружен ли список ссылок
func CheckRequestData_checkloadlist(info string, body []byte) bool {
	buff := bytes.NewBuffer(body)
	doc, err := goquery.NewDocumentFromReader(buff)
	LogPrintErrAndExit("goquery.NewDocumentFromReader error: \n"+info+"", err)

	//--------------------------------------------------------------------------
	//ищем блок с ссылками на адреса:
	table := doc.Find(".brdw1111")
	if table.Length() == 0 {
		return false
	}
	return true
}

//отправляем запрос на список адресов по дому
func SendHttpRequestPOST_loadlist(urlStr string, m map[string]string) ([]byte, map[string]interface{}) {
	data := url.Values{}

	//settlement_id := "-1"
	//settlement := ""

	street := m["street"]

	//--------------------------------------------------------------------------
	/***************************
	//обязательно должны быть заданы все следующие поля(могут быть пустые но должны быть):
	для города                                 для абези:
	search_action:true                         search_action:true                   search_action:true
	subject:                                   subject:                             subject:
	region:                                    region:                              region:
	settlement:                                settlement:187415802001              settlement:
	cad_num:                                   cad_num:                             cad_num:
	start_position:59                          start_position:59                    start_position:59
	obj_num:                                   obj_num:                             obj_num:
	old_number:                                old_number:                          old_number:
	search_type:ADDRESS                        search_type:ADDRESS                  search_type:ADDRESS
	src_object:1                               src_object:1                         src_object:1
	subject_id:187000000000                    subject_id:187000000000              subject_id:118000000000
	region_id:187415000000                     region_id:187415000000               region_id:118401000000
	settlement_type:-1                         settlement_type:-1
	settlement_id:-1                           settlement_id:187415802001
	street_type:str0                           street_type:str0                     street_type:str0
	street:Мира                                street:Вокзальная                    street:гагринская
	house:69                                   house:3                              house:1
	building:                                  building:                            building:
	structure:                                 structure:                           structure:
	apartment:                                 apartment:                           apartment:
	r_subject_id:101000000000                  r_subject_id:101000000000
	right_reg:                                 right_reg:                           right_reg:
	encumbrance_reg:                           encumbrance_reg:                     encumbrance_reg:
	****************************/
	data.Set("search_action", "true")
	data.Add("subject", "")
	data.Add("region", "")
	data.Add("settlement", "")
	data.Add("cad_num", "")
	data.Add("start_position", "59")
	data.Add("obj_num", "")
	data.Add("old_number", "")
	data.Add("search_type", "ADDRESS")
	data.Add("src_object", "1")
	data.Add("subject_id", "118000000000") // 118000000000 - волгоградская обл., 187000000000 - коми
	data.Add("region_id", "118401000000")  // 118401000000 - волгоград,  187415000000 - инта
	data.Add("settlement_type", "-1")
	data.Add("settlement_id", "")
	data.Add("street_type", "str0")
	data.Add("street", street)
	data.Add("house", m["house"])
	data.Add("building", "")
	data.Add("structure", "")
	data.Add("apartment", "")
	//data.Add("r_subject_id", "") //101000000000 - hz
	data.Add("right_reg", "")
	data.Add("encumbrance_reg", "")
	//LogPrint(fmt.Sprintf("\ndata: %#v\n", data))
	//--------------------------------------------------------------------------

	data_enc := data.Encode()

	cookieJar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar: cookieJar,
	}

	r, err := http.NewRequest("POST", urlStr, bytes.NewBufferString(data_enc))
	LogPrintErrAndExit("http.NewRequest error: \n urlStr: "+urlStr+"\n\n", err)
	//r.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(data_enc)))

	//r.Header.Add("Connection", "keep-alive")
	//r.Header.Add("Cache-Control", "max-age=0")
	//r.Header.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4")
	//r.Header.Add("Upgrade-Insecure-Requests", "1")
	r.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")

	resp, err := client.Do(r)
	LogPrintErrAndExit("http.NewRequest.client.Do error: \n urlStr: "+urlStr+"\n\n", err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	LogPrintErrAndExit("http.resp.Body Read error: \n urlStr: "+urlStr+"\n\n", err)

	d := map[string]interface{}{"Referer": resp.Header.Get("Referrer")}
	if d["Referer"].(string) == "" {
		d["Referer"] = urlStr
	}
	d["cookie"] = cookieJar //куки нужны для просмотра след.страниц
	return body, d
}

//получаем просто результат гет запроса:
func SendHttpRequestGET(info string, urlStr string, d map[string]interface{}) []byte {
	//cookieJar, _ := cookiejar.New(nil)
	//time.Sleep(time.Second * 1)

	client := &http.Client{
		Jar: d["cookie"].(http.CookieJar),
	}

	r, err := http.NewRequest("GET", urlStr, nil)
	LogPrintErrAndExit("SendHttpRequestGET: http.NewRequest error: \n "+info+" \n urlStr: "+urlStr+"\n\n", err)
	//r.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	//LogPrint("Referer: " + d["Referer"].(string))
	r.Header.Add("Referer", d["Referer"].(string))

	//r.Header.Add("Content-Length", strconv.Itoa(len(data_enc)))

	resp, err := client.Do(r)
	LogPrintErrAndExit("SendHttpRequestGET: http.NewRequest.client.Do error: \n "+info+" \n urlStr: "+urlStr+"\n\n", err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	LogPrintErrAndExit("SendHttpRequestGET: http.resp.Body Read error: \n "+info+" \n urlStr: "+urlStr+"\n\n", err)

	//mf.FileWrite("test/"+info+".res", body)

	referer := resp.Header.Get("Referrer")
	if referer == "" {
		referer = urlStr
	}
	d["Referer"] = urlStr
	return body
}

//разбираем строку на ул,дом,кв
//Россия, Волгоградская обл., г. Волгоград, ул. Гагринская, дом №1, кв. 91
// г Волгоград, ул Гагринская, д. 1, 34
func parse_link_adres_text(s string) (string, string, string) {
	s = strings.ToLower(mf.StrTrim(s))
	/*****
	s = strings.Replace(s, "россия,", "", -1)
	s = strings.Replace(s, "волгоградская,", "", -1)
	s = strings.Replace(s, "область", "", -1)
	s = strings.Replace(s, "обл.", "", -1)
	s = strings.Replace(s, " обл,", "", -1)

	s = strings.Replace(s, "дом №", "", -1)
	s = strings.Replace(s, "дом№", "", -1)
	s = strings.Replace(s, "дом№", "", -1)

	s = strings.Replace(s, "помещение", "", -1)
	*******/

	a := strings.Split(s, ",")
	if len(a) < 2 {
		return s, "", ""
	}

	ss := a[len(a)-2]
	sh := a[len(a)-1]
	sf := ""

	if len(a) > 3 {
		ss = a[len(a)-3]
		sh = a[len(a)-2]
		sf = a[len(a)-1]
	}

	s = ss + "|" + sh + "|" + sf

	s = strings.Replace(s, "улица", "", -1)
	s = strings.Replace(s, "ул.", "", -1)
	s = strings.Replace(s, "(ул)", "", -1)
	s = strings.Replace(s, "пр - кт", "", -1)
	s = strings.Replace(s, "пр-кт", "", -1)
	s = strings.Replace(s, " проспект ", "", -1)
	s = strings.Replace(s, "пр.", "", -1)
	s = strings.Replace(s, "пр ", "", -1)

	s = strings.Replace(s, "дом №", " ", -1)
	s = strings.Replace(s, "дом", " ", -1)
	s = strings.Replace(s, "здание", " ", -1)
	s = strings.Replace(s, "зд.", " ", -1)
	s = strings.Replace(s, "зд ", " ", -1)
	s = strings.Replace(s, "д.", " ", -1)
	s = strings.Replace(s, "№", " ", -1)

	s = strings.Replace(s, "кв.", " ", -1)
	s = strings.Replace(s, "квартира", " ", -1)
	s = strings.Replace(s, "кв", " ", -1)
	s = strings.Replace(s, "помещение", " ", -1)
	s = strings.Replace(s, "пом.", " ", -1)
	s = strings.Replace(s, "пом", " ", -1)

	s = mf.StrTrim(s)
	s = mf.StrRegexpReplace(s, "^ул ", "")
	s = mf.StrRegexpReplace(s, "^пер ", "")
	s = strings.Replace(s, "()", "", -1)

	s = mf.StrTrim(s)

	a = strings.Split(s, "|")

	a[0] = mf.StrTrim(a[0])
	a[1] = mf.StrTrim(a[1])
	a[2] = mf.StrTrim(a[2])

	//если a[1] не номер дома то это улица а номер дома следующее значение
	if mf.StrRegexpMatch("\\d{1,3}.*", a[1]) == false {
		a[0] = a[1]
		a[1] = a[2]
		a[2] = ""
	}

	return a[0], a[1], a[2]
}

//получаем список ссылок по адресам со всех страниц рекурсивно загружая эти страницы
func loadpagelinks(info string, page int, body []byte, alist *urllist, d map[string]interface{}) {
	//mf.FileWrite("test/"+info+".res"+strconv.Itoa(page), body)
	buff := bytes.NewBuffer(body)
	doc, err := goquery.NewDocumentFromReader(buff)
	LogPrintErrAndExit("goquery.NewDocumentFromReader error: \n"+info+" page:"+strconv.Itoa(page), err)

	//--------------------------------------------------------------------------
	//ищем блок с ссылками на адреса:
	table := doc.Find(".brdw1111")
	if table.Length() == 0 {
		mf.FileWrite("log/"+info+".ERR", body)
		LogPrint("ERROR3001 not found elements for selector '.brdw1111'  " + info + " page:" + strconv.Itoa(page))
		return
	}

	a := table.Find(".td").Find("a")
	if a.Length() == 0 {
		mf.FileWrite("log/"+info+".ERR", body)
		LogPrintAndExit("not found elements for selector  '.brdw1111'>'.td'>'.a'  " + info + " page:" + strconv.Itoa(page))
	}

	baseurl := getbaseurl(doc)

	//LogPrint(fmt.Sprintf("t:%d    td:%d    a:%d", table.Length(), td.Length(), a.Length()))
	//сохраняем ссылки на адреса в alist:
	for i := 0; i < a.Length(); i++ {
		ai := a.Eq(i)
		if ai.Length() == 0 {
			LogPrint(fmt.Sprintf("skip1 i:%d a:%s", i, ai.Text()))
			continue
		}
		text := mf.StrTrim(ai.Text())
		href, _ := ai.Attr("href")
		b := strings.Index(href, "object_data_id=")
		if b == -1 {
			LogPrint(fmt.Sprintf("skip2 i:%d a:%s", i, text))
			continue
		}
		m := map[string]string{"url": baseurl + href, "name": text}

		m["street"], m["house"], m["flat"] = parse_link_adres_text(text)

		alist.add(m)
		LogPrint(fmt.Sprintf("ok i:%d a:%s, [%s, %s-%s]", i, text, m["street"], m["house"], m["flat"]))
		//mf.FileWrite("test/"+text+".a", []byte(href))
	}
	//LogPrint(fmt.Sprintf("loaded list: %d\n\n", len(alist.a)))

	//--------------------------------------------------------------------------
	//ищем ссылку на следующую страницу:
	a2 := table.Find("table").Find("table").Find("a")
	if a2.Length() == 0 {
		//кнопок переключения страниц просто нет
		return
		//LogPrintAndExit("not found elements for selector  '.brdw1111'>'table'>'table'>'a'  " + info + " page:" + strconv.Itoa(page))
	}

	NextPagestr := strconv.Itoa(page + 1)
	needHref := ""

	for i := 0; i < a2.Length(); i++ {
		ai := a2.Eq(i)
		if ai.Length() == 0 {
			LogPrint(fmt.Sprintf("skip1 i:%d a:%s", i, ai.Text()))
			continue
		}
		text := mf.StrTrim(ai.Text())
		href, _ := ai.Attr("href")
		b := strings.Index(href, "online_request_search_page=")
		if b == -1 {
			LogPrint(fmt.Sprintf("skip2 i:%d a:%s", i, text))
			continue
		}
		if text == NextPagestr {
			needHref = baseurl + href
			break
		}
		//LogPrint(fmt.Sprintf("page: %s\n\n", text))
	}

	//если ссылку на след. страницу не нашли то выходим из функции
	if needHref == "" {
		return
	}

	//загружаем следующую страницу
	nextpagebody := SendHttpRequestGET(info+"_page"+strconv.Itoa(page+1), needHref, d)

	loadpagelinks(info, page+1, nextpagebody, alist, d)
}

//загружаем данные по отдельной квартире-дому и сохраняем в map[string]interface{}
func loadkadastrinfo(info string, urlStr string, d map[string]interface{}) map[string]string {
	body := SendHttpRequestGET(info, urlStr, d)
	buff := bytes.NewBuffer(body)
	doc, err := goquery.NewDocumentFromReader(buff)
	LogPrintErrAndExit("loadkadastrinfo: goquery.NewDocumentFromReader error: \n"+info+":\n", err)

	t := doc.Find(".brdw1010").Find("table").Eq(0)
	if t.Length() == 0 {
		mf.FileWrite("log/"+info+".ERR", body)
		LogPrintAndExit("not found elements for selector '.brdw1010'>'table'  " + info)
	}

	ret := make(map[string]string)
	ret["kadastrn"] = findTrFieldByText(info, "Кадастровый номер", ".*", t, "-", true)
	ret["ob_area"] = strings.Replace(findTrFieldByText(info, "Площадь ОКС", "^[\\d\\.,]+$", t, "0", false), ",", ".", -1)
	ret["type"] = strings.ToLower(findTrFieldByText(info, "(ОКС) Тип:", ".*", t, "-", false))
	ret["price"] = strings.Replace(findTrFieldByText(info, "Кадастровая стоимость", "^[\\d\\.,]+$", t, "0", false), ",", ".", -1)
	ret["price_date"] = findTrFieldByText(info, "Дата утверждения стоимости", "^[\\d\\.-]+$", t, "-", false)
	ret["price_date_enter"] = findTrFieldByText(info, "Дата внесения стоимости", "^[\\d\\.-]+$", t, "", false)
	ret["price_date_check"] = findTrFieldByText(info, "Дата определения стоимости", "^[\\d\\.-]+$", t, "", false)

	ret["floor_cnt"] = findTrFieldByText(info, "Этажность", "^\\d+$", t, "0", false)
	ret["ufloor_cnt"] = findTrFieldByText(info, "одземн", "^\\d$", t, "0", false)
	ret["wall_info"] = findTrFieldByText(info, "Материал стен", ".*", t, "0", false)
	ret["build_year"] = findTrFieldByText(info, "Завершение строительства", "^[\\d\\.-]+$", t, "0", false)
	ret["build_year2"] = findTrFieldByText(info, "Ввод в эксплуатацию", "^[\\d\\.-]+$", t, "0", false)
	ret["date_update"] = findTrFieldByText(info, "Дата обновления информации", "^[\\d\\.-]+$", t, "0", false)
	ret["date_kadastr"] = findTrFieldByText(info, "Дата постановки на кадастровый учет", "^[\\d\\.-]+$", t, "0", false)

	ret["date_kadastr_end"] = findTrFieldByText(info, "Дата снятия с учета", "^[\\d\\.-]+$", t, "", false)
	ret["status"] = findTrFieldByText(info, "Статус объекта", ".*", t, "", false)
	ret["adres"] = findTrFieldByText(info, "Адрес (местоположение)", ".*", t, "", false)

	ret["flat"] = "-"
	ret["type"] = mf.StrRegexpReplace(ret["type"], "[\\t\\n\\r ]+", " ")
	if mf.StrRegexpMatch("квартира", ret["type"]) || mf.StrRegexpMatch("помещение", ret["type"]) {
		flat := findTrFieldByText(info, "Адрес (местоположение)", ".*", t, "-", false)
		ret["flat"] = clearTextAddress(flat)
		type_flat := str_get_number_only(ret["type"])
		//type_flat = mf.StrTrim(type_flat)
		//LogPrint(fmt.Sprintf("flat / type_flat: [%v] / [%v]", flat, type_flat))
		if type_flat != "" && mf.StrRegexpMatch("^\\d+$", type_flat) {
			ret["flat"] = type_flat
			//ret["type"] = "квартира"
		}
		//Жилое помещение  (Комната №177)
	}

	//LogPrint(fmt.Sprintf(info+" %v", ret))

	//Кадастровый номер
	//LogPrintAndExit(t.Text())
	return ret
}

//возвращает пустую структуру со всеми но пустыми полями
func loadkadastrinfo_null() map[string]string {
	ret := make(map[string]string)
	ret["kadastrn"] = "-"
	ret["ob_area"] = "0"
	ret["type"] = ""
	ret["price"] = "0"
	ret["price_date"] = ""

	ret["floor_cnt"] = "0"
	ret["ufloor_cnt"] = "0"
	ret["wall_info"] = ""
	ret["build_year"] = "0"
	ret["build_year2"] = "0"
	ret["date_update"] = ""
	ret["date_kadastr"] = ""

	ret["date_kadastr_end"] = ""
	ret["status"] = ""
	ret["adres"] = ""

	ret["flat"] = ""
	return ret
}

//возвращает текст из соседнего td поля
func findTrFieldByText(info, text, testRegexp string, s *goquery.Selection, defaultval string, isrequired bool) string {
	a := s.Find("tr:contains(\"" + text + "\")")
	if a.Length() == 0 {
		if isrequired {
			//body, _ := s.Html()
			//mf.FileWrite("log/"+info+".ERR", []byte(body))
			LogPrintAndExit("not found elements for selector '.brdw1010'>'table'>tr:contains(\"" + text + "\")  " + info)
		}
		return defaultval
	}
	a = a.Find("td")
	if a.Length() < 2 {
		if isrequired {
			//body, _ := s.Html()
			//mf.FileWrite("log/"+info+".ERR", []byte(body))
			LogPrintAndExit("not found elements for selector '.brdw1010'>'table'>tr:contains(\"" + text + "\")>td" + info)
		}
		return defaultval
	}
	t := mf.StrTrim(a.Eq(1).Text())
	if t == "" {
		if isrequired {
			//body, _ := s.Html()
			//mf.FileWrite("log/"+info+".ERR", []byte(body))
			LogPrintAndExit("no text in element for selector '.brdw1010'>'table'>tr:contains(\"" + text + "\")>td" + info)
		}
		return defaultval
	}
	if mf.StrRegexpMatch(testRegexp, t) == false {
		if isrequired {
			//body, _ := s.Html()
			//mf.FileWrite("log/"+info+".ERR", []byte(body))
			LogPrintAndExit("not match text(\"" + t + "\") in element for selector '.brdw1010'>'table'>tr:contains(\"" + text + "\")>td" + info)
		}
		return defaultval
	}
	return t
}

//возвращает текст после последней запятой с удаленными лишними символами "кв." "кв" и т.д.
func clearTextAddress(text string) string {
	s := strings.ToLower(mf.StrTrim(text))
	i := strings.LastIndex(s, ",")
	if i < 0 {
		//return s
		i = -1
	}
	s = s[i+1:]

	s = strings.Replace(s, ".", "", -1)
	s = strings.Replace(s, "-", "", -1)
	s = strings.Replace(s, "нежилое", "", -1)
	s = strings.Replace(s, "жилое", "", -1)
	s = strings.Replace(s, "помещение", "", -1)
	s = strings.Replace(s, "квартира", "", -1)
	s = strings.Replace(s, "кварт", "", -1)
	s = strings.Replace(s, "квар", "", -1)
	s = strings.Replace(s, "квра", "", -1)
	s = strings.Replace(s, "кв", "", -1)
	s = strings.Replace(s, "помещение", "", -1)
	s = strings.Replace(s, "помещен", "", -1)
	s = strings.Replace(s, "помещ", "", -1)
	s = strings.Replace(s, "пом", "", -1)
	s = strings.Replace(s, "п-е", "", -1)
	s = strings.Replace(s, "п", "", -1)
	s = strings.Replace(s, "номер", "", -1)
	s = strings.Replace(s, "ном", "", -1)
	s = strings.Replace(s, "н", "", -1)
	s = strings.Replace(s, "№", "", -1)
	s = strings.Replace(s, "(", "", -1)
	s = strings.Replace(s, ")", "", -1)

	s = mf.StrTrim(s)
	s = strings.Trim(s, "\r\n\t ")
	if s != "" && s[0:1] == "0" {
		s = s[1:]
	}
	return s
}

func str_get_number_only(text string) string {
	s := mf.StrRegexpReplace(text, "[^\\d]+", "")
	return s
}
