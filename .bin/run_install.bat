﻿::получаем curpath:
@FOR /f %%i IN ("%0") DO SET curpath=%~dp0
::задаем основные переменные окружения
@CALL "%curpath%/set_path.bat"


@del app.exe
@CLS

@echo === install ===================================================================
go get "github.com/nakagami/firebirdsql"
go get -u "github.com/mixamarciv/gofncstd3000"
go get "github.com/parnurzeal/gorequest"
go get "github.com/jessevdk/go-flags"

::библиотека для работы с XMLками
::go get -u "github.com/jteeuwen/go-pkg-xmlx"

::библиотека для перевода struct в map[string]interface{}
go get github.com/fatih/structs

::библиотека для работы с html
go get "github.com/PuerkitoBio/goquery"

::xls чтение
::go get "github.com/extrame/xls"
::go get "github.com/tealeg/xlsx"
go get github.com/nivrrex/excel

::xls запись 
go get "github.com/Luxurioust/excelize"


go install

@echo ==== end ======================================================================
@PAUSE
